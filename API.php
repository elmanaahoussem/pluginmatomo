<?php
/**
 * Matomo - free/libre analytics platform
 *
 * @link https://matomo.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Piwik\Plugins\CustomPlugin;

use Piwik\DataTable;
use Piwik\DataTable\Row;

/**
 * API for plugin CustomPlugin
 *
 * @method static \Piwik\Plugins\CustomPlugin\API getInstance()
 */
class API extends \Piwik\Plugin\API
{

    /**
     * Another example method that returns a data table.
     * @param int    $idSite
     * @param string $period
     * @param string $date
     * @param bool|string $segment
     * @return DataTable
     */

    public function getLastVisitsByBrowser($idSite, $period, $date, $segment = false)
    {

        $data = \Piwik\API\Request::processRequest('Live.getLastVisitsDetails', array(
            'idSite' => $idSite,
            'period' => $period,
            'date' => $date,
            'segment' => $segment,
            'numLastVisitorsToFetch' => 100,
            'minTimestamp' => false,
            'flat' => false,
            'doNotFetchActions' => true,

        ));
        $data->applyQueuedFilters();
        // we could create a new instance by using new DataTable(),
        // but we would lose DataTable metadata, which can be useful.
        $result = $data->getEmptyClone($keepFilters = false);

        foreach ($data->getRows() as $visitRow) {
            $browserName = $visitRow->getColumn('browserName');

            // try and get the row in the result DataTable for the browser
            $browserRow = $result->getRowFromLabel($browserName);

            // if there is no row for this browser, create it
            if ($browserRow === false) {

                $result->addRowFromSimpleArray(array(
                    'label'     => $browserName,
                    'nb_visits' => 1
                ));


            } else { // if there is a row, increment the counter
                $counter = $browserRow->getColumn('nb_visits');
                $browserRow->setColumn('nb_visits', $counter + 1);
            }
        }

        return $result;
    }



}
